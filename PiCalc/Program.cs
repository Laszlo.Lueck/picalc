﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Diagnostics.Contracts;
using System.Globalization;
using System.Linq;
using System.Numerics;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;
using PiCalc;


if (args.Length == 0)
{
    Console.WriteLine("Not a given iteration value");
    Environment.Exit(0);
}

var precision = 16;
var parallelism = 1;
var type = 1;
for (var i = 1; i < args.Length; i++)
{
    switch (args[i])
    {
        case var pc when pc.StartsWith("p:"):
            var pValOpt = pc.Replace("p:", "").Trim();
            if (int.TryParse(pValOpt, out var bol))
                parallelism = bol;
            break;
        case var pp when pp.StartsWith("c:"):
            var ppValOpt = pp.Replace("c:", "").Trim();
            if (int.TryParse(ppValOpt, out var it))
                precision = it;
            break;
        case var pt when pt.StartsWith("t:"):
            var ptValOpt = pt.Replace("t:", "").Trim();
            if (int.TryParse(ptValOpt, out var ip))
                type = ip;
            break;
    }
}

Console.WriteLine($"working with precision of {precision}");

if (long.TryParse(args[0], out var lValue))
{
    var sw = Stopwatch.StartNew();

    var bd = type switch
    {
        1 => parallelism == 1 ? Calculate.GetPiByNilakanthaMt(lValue).ToString(CultureInfo.CurrentCulture) : Calculate.GetPiByNilakantha(lValue).ToString(CultureInfo.CurrentCulture),
        2 => Calculate.GetPiByChudnovsky(precision, (int)lValue).ToString().AddDecimalSign(1, Calculate.GetDecimalSign()),
        _ => throw new ArgumentOutOfRangeException(nameof(type))
    };

    var elapsed = sw.Elapsed.TotalMilliseconds;
    var consoleValue = parallelism == 1 ? "multi threaded mode" : "single threaded mode";
    var piType = type == 1 ? "Nilakantha" : "Chudnovsky";
    Console.WriteLine($"Pi by {piType} with {lValue} iterations in {consoleValue} in {elapsed}ms is ");
    Console.WriteLine($"{bd}");
}
else
{
    Console.WriteLine($"Not a long value given for {args[0]}");
}

internal static class Calculate
{
    internal static readonly Func<string> GetDecimalSign =
        () => CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;

    internal static string AddDecimalSign(this string source, int position, string sign) =>
        source.Insert(position, sign);

    internal static readonly Func<long, decimal> GetPiByNilakantha = n => 3 + GenerateLongSequence(n)
        .InternalCalc()
        .Aggregate<decimal, decimal>(0, (current, bd) => current + bd);

    internal static readonly Func<long, decimal> GetPiByNilakanthaMt = n => 3 + GenerateLongSequence(n)
        .AsParallel()
        .InternalCalc()
        .Aggregate<decimal, decimal>(0, (current, bd) => current + bd
        );

    private static IEnumerable<decimal> InternalCalc(this IEnumerable<long> source) =>
        source.Select(InternalCalcFnc);

    private static ParallelQuery<decimal> InternalCalc(this ParallelQuery<long> source) =>
        source.Select(InternalCalcFnc);

    private static readonly Func<long, decimal> InternalCalcFnc =
        i => (i % 2 == 0 ? 1 : -1) * 4m / ((2m + i * 2) * (3 + i * 2) * (4 + i * 2));


    private static IEnumerable<long> GenerateLongSequence(long n)
    {
        for (long i = 0; i < n; i++)
            yield return i;
    }

    private static IEnumerable<int> GenerateIntSequence(int startValue, int endInclusive)
    {
        for (var i = startValue; i <= endInclusive; i++)
            yield return i;
    }


    private static readonly ConcurrentDictionary<int, BigInteger> FacMap = new();

    private static readonly Func<int, BigInteger> Factorial = source =>
    {
        return FacMap.GetOrAdd(source, key =>
        {
            return GenerateIntSequence(1, key)
                .AsParallel()
                .Aggregate(BigInteger.One, (acc, val) => acc * val);
        });
    };


    internal static readonly Func<int, int, BigInteger> GetPiByChudnovsky = (precision, iterations) =>
    {
        Console.WriteLine("start crunching ...");
        Console.WriteLine();

        BigInteger constA = new(13591409);
        BigInteger constB = new(545140134);
        BigInteger constC = new(-640320);
        BigInteger twelve = new(12);

        var f = BigInteger.Pow(10, precision);
        var sqrtTerm = BigInteger.Pow(640320, 3) * BigInteger.Pow(f, 2);

        var z = GenerateIntSequence(0, iterations)
            .AsParallel()
            .Select(k => Calculator(k, f, constA, constB, constC))
            .Aggregate(new BigInteger(0), (acc, term) => acc + term);
        Console.WriteLine("");
        Console.WriteLine("...finished crunching");

        return BigInteger.Pow(f, 2) / (twelve * z * f / sqrtTerm.NewtonPlusSqrt());
    };

    private static readonly Func<int, BigInteger, BigInteger, BigInteger, BigInteger, BigInteger> Calculator =
        (k, bigInteger, constA1, constB1, constC1) =>
            bigInteger * Factorial(6 * k) * (constA1 + constB1 * new BigInteger(k)) /
            (Factorial(3 * k) * BigInteger.Pow(Factorial(k), 3) * BigInteger.Pow(constC1, 3 * k));
}